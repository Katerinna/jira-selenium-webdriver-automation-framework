package com.telerikacademy.testframework.pages;

public class JiraConstants {
    public static final String STORY_SUMMARY = "See the information about the company from 'about us'";
    public static final String STORY_DESCRIPTION = "Description:\n" +
            "Open the page with the information about the travel company  \n" +
            "Narrative \n" +
            "As a user to PHPTRAVELS | Travel Technology Partner \n" +
            "I want to be able to read some information about the company\n" +
            "So that i can understand more about the company ";

    public static final String BUG_SUMMARY = "About us page has no information about the company";
    public static final String BUG_DESCRIPTION = "Steps to reproduce:\n" +
            "1.Navigate to PHPTRAVELS | Travel Technology Partner \n" +
            "2.Scroll down to the bottom of the page and click About us\n" +
            "Expected result: A page with information about the company is displayed\n" +
            "Actual result: Company about us page is not displayed and you are redirected to the page which prompts you " +
            "to use the mobile app\n" +
            "Severity: Medium\n" +
            "Prerequisites: N/A";

    public static final String BUG_ENVIRONMENT = "Mozilla Firefox browser";
}


