package pages.jira;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.pages.JiraConstants.STORY_DESCRIPTION;
import static com.telerikacademy.testframework.pages.JiraConstants.STORY_SUMMARY;

public class CreateStoryPage extends BaseJiraPage {


    public CreateStoryPage(WebDriver webDriver){
        super(webDriver,"jira.project.page");
    }

    public void assertInCorrectProject() {
        actions.waitForElementVisible("jira.projectPage.projectName");
        actions.assertElementPresent("jira.projectPage.projectName");
    }

    public void clickOnCreateButton(){
        actions.waitForElementVisibleUntilTimeout("jira.projectPage.createButton",10);
        actions.clickElement("jira.projectPage.createButton");
    }

    public void issueTypeDropDownButton(){
        actions.waitForElementVisible("jira.issuePage.issueTypeButton");
        actions.clickElement("jira.issuePage.issueTypeButton");
        actions.clickElement("jira.issuePage.issueTypeButton.selectStory");
    }

    public void addSummary(){
        actions.waitForElementVisible("jira.issuePage.addSummary");
        actions.typeValueInField(STORY_SUMMARY,"jira.issuePage.addSummary");
    }

    public void addDescription(){
        actions.waitForElementVisible("jira.issuePage.addDescription");
        actions.typeValueInField(STORY_DESCRIPTION,"jira.issuePage.addDescription");
    }
    public void selectAssignee(){
        actions.waitForElementVisible("jira.issuePage.assigneeButton");
        actions.clickElement("jira.issuePage.assigneeButton");
    }
    public void selectPriority(){
        actions.waitForElementVisible("jira.issuePage.selectPriority");
        actions.clickElement("jira.issuePage.selectPriority");
        actions.clickElement("jira.issuePage.priorityOptionsStory");

    }

    public void clickCreateIssueButton(){
        actions.clickElement("jira.issuePage.createIssueButton");
    }

    public void assertStoryAddedToTheBackLog(){
        actions.waitForElementVisible("jira.projectPage.issueAddedToTheBackLog");

    }

}
