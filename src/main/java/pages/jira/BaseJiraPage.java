package pages.jira;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public abstract class BaseJiraPage extends BasePage {

    public BaseJiraPage(WebDriver webDriver,String pageUrl){
        super(webDriver,pageUrl);
    }
}
